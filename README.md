# Gravity

Just a simple gravity simulation attempt for fun in UE4

Gameplay: 
    The ethereal player zooms around space with a star in the level that will attract objects spawned by the player.
    The player first clicks on a position that deprojects to a plane aligned with the start on a XY axis.
    While holding the mouse down, moving the mouse around determines the velocity and direction the object gets spawned with.
    Size of the planetoid depends on the amount of time the player keeps the mouse button pressed - longer the press, the bigger
    the planetoid.
    Planetoids that crash into the star feed it and make its gravity stronger.
