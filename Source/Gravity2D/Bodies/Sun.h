// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GravityGameInstance.h"
#include "Sun.generated.h"


class UStaticMeshComponent;

UCLASS()
class GRAVITY2D_API ASun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASun();

	ASun* GetSunPointer()  { return this; };

	float GetRadialForceStrength() const { return RadialForceStrength; };
	/// We'll increment the force by just adding the mass of the planetoids that collide with the sun (to lazy to brain with density etc)
	void IncreaseRadialForceStrength(const float _extraForce) { RadialForceStrength += _extraForce; };

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditAnywhere)
	bool bApplyRadialForce;

	UPROPERTY(EditAnywhere)
	float ForceRadius;

	UPROPERTY(EditAnywhere)
	float RadialForceStrength;

	UGravityGameInstance *GameInstance = nullptr;

	FVector SunExtent, SunOrigin;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* BodyMesh = nullptr;
	
};
