// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Planetoid.generated.h"

UCLASS()
class GRAVITY2D_API APlanetoid : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlanetoid();

	float GetPlanetoidDensity() const { return PhysicalDensity; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* BodyMesh = nullptr;

	// The physical density of the "material" the planetoid is made of
	UPROPERTY(EditAnywhere)
	float PhysicalDensity;

	~APlanetoid();


};
