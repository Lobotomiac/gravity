// Fill out your copyright notice in the Description page of Project Settings.

#include "Sun.h"
#include "Components/StaticMeshComponent.h"
#include "DrawDebugHelpers.h"

#include "Game/GravityGameMode.h"


// Sets default values
ASun::ASun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BodyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BodyMesh"));
	BodyMesh->OnComponentHit.AddDynamic(this, &ASun::OnHit);
	BodyMesh->SetNotifyRigidBodyCollision(true);
	BodyMesh->SetSimulatePhysics(true);
	RootComponent = BodyMesh;

	bApplyRadialForce = true;
	RadialForceStrength = -1000.0f;
	ForceRadius = 1000.0f;
}

// Called when the game starts or when spawned
void ASun::BeginPlay()
{
	Super::BeginPlay();

	// We're giving a pointer to our Sun actor to the GameMode for easier access for other Objects
	AGravityGameMode* GameMode = Cast<AGravityGameMode>(GetWorld()->GetAuthGameMode());
	GameMode->SetSun(GetSunPointer());

	GameInstance = Cast<UGravityGameInstance>(GetWorld()->GetGameInstance()); 
	GetActorBounds(false, SunOrigin, SunExtent); 	// Instead of GetActorLocation, this way we get the real center of the SunActor
	DrawDebugSphere(GetWorld(), SunOrigin, ForceRadius, 32, FColor::Red, true); //TODO put this in BeginPlay to not waste frames

}

// Called every frame
void ASun::Tick(float DeltaTime)
{
	if (GameInstance->Planetoids.Num() >  0)
	{
		for (auto Planetoid : GameInstance->Planetoids)
		{
			UStaticMeshComponent *Mesh = Cast<UStaticMeshComponent>(Planetoid->GetRootComponent());
			if (Mesh && Mesh->IsSimulatingPhysics()) 
			{
				// Making this a negative force to make it attract rather than push objects away
				Mesh->AddRadialForce(SunOrigin, ForceRadius, (RadialForceStrength * -1), ERadialImpulseFalloff::RIF_Linear, false);
				//TODO if actor is further than range of force by Sun, destroy it - the Sun is no longer the main gravitational force
			}
		}
	}
}

void ASun::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	UE_LOG(LogTemp, Warning, TEXT("OnHit Called ON THE FREAKING SUN"))
	if (OtherActor && OtherActor->IsA(APlanetoid::StaticClass()) && OtherComp)
	{
		UStaticMeshComponent* mesh = Cast<UStaticMeshComponent>(OtherComp);
		float AdditionalForce = mesh->GetMass();
		IncreaseRadialForceStrength(AdditionalForce);

		APlanetoid* planetoid = Cast<APlanetoid>(OtherActor);

		GameInstance->RemovePlanetoid(planetoid);

		OtherActor->Destroy();
	}
}

