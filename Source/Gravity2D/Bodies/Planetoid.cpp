// Fill out your copyright notice in the Description page of Project Settings.

#include "Planetoid.h"
#include "GravityGameInstance.h"


// Sets default values
APlanetoid::APlanetoid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BodyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body"));
	BodyMesh->SetSimulatePhysics(false);
	BodyMesh->SetWorldScale3D(FVector(0.01f, 0.01f, 0.01f));
	RootComponent = BodyMesh;

	// Approximately the default density of standard meshes in UE4
	PhysicalDensity = 4.78351313;
	
}

// Called when the game starts or when spawned
void APlanetoid::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("Planetoid Created!"))

	UGravityGameInstance *GameInstance = Cast<UGravityGameInstance>(GetWorld()->GetGameInstance());
	GameInstance->AddPlanetoid(this);
	//TODO start a timer to see how long can it keep from falling into the sun
	
}



APlanetoid::~APlanetoid()
{
	
	UE_LOG(LogTemp, Warning, TEXT("DESTRUCTOR CALLED ON %s"), *this->GetName())
}