// Fill out your copyright notice in the Description page of Project Settings.

#include "GravityGameInstance.h"



// include Planetoids and Sun
// Create delegate for when planetoids spawn

void UGravityGameInstance::AddPlanetoid(const APlanetoid* _planetoid)
{
	Planetoids.Add(_planetoid);
	UE_LOG(LogTemp, Warning, TEXT("Added a planetoid"))

	// TODO setup a cleanup for dangling pointers after a planetoid has been destroyed
}

void UGravityGameInstance::RemovePlanetoid(const APlanetoid* _planetoid)
{
	int HasRemoved = Planetoids.RemoveSingle(_planetoid);
	if (HasRemoved)
	{
		UE_LOG(LogTemp, Warning, TEXT("Removed a planetoid"))
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to remove a planetoid"))

	}
}