// Fill out your copyright notice in the Description page of Project Settings.

#include "GravityPlayerController.h"
#include "Bodies/Sun.h"



AGravityPlayerController::AGravityPlayerController() : APlayerController()
{
	bEnableClickEvents = true;
	bShowMouseCursor = true;
	bEnableMouseOverEvents = true;

	bEnableTouchEvents = false;
	
}


void AGravityPlayerController::BeginPlay()
{
	Super::BeginPlay();

	Super::SetupInputComponent();

	InputComponent->BindAction("MouseLeftClick", IE_Pressed, this, &AGravityPlayerController::MouseButtonLeftPressed);
	InputComponent->BindAction("MouseLeftClick", IE_Released, this, &AGravityPlayerController::MouseButtonLeftReleased);

}

void AGravityPlayerController::MouseButtonLeftPressed()
{
	PlayerClicked.ExecuteIfBound();
	//UE_LOG(LogTemp, Warning, TEXT("MouseButtonLeftPressed"));


	// TODO disable camera movement while mouse button down

}

void AGravityPlayerController::MouseButtonLeftReleased()
{
	PlayerClickReleased.ExecuteIfBound();
	//UE_LOG(LogTemp, Warning, TEXT("MouseButtonLeftReleased"));
}

