// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GravityPlayerController.generated.h"

/**
 * Check how it looks as a free operated camera (as is 10.11.2020.)
 * (when it is playable of course) ?? how the fuck am I supposed to know what I meant by this shit
 */




DECLARE_DELEGATE(FPlayerClickDelegate);


UCLASS()
class GRAVITY2D_API AGravityPlayerController : public APlayerController
{
	GENERATED_BODY()

	// Lets construct this thing
	AGravityPlayerController();

	virtual void BeginPlay() override;

	// Ping click
	UFUNCTION()
	void MouseButtonLeftPressed();

	UFUNCTION()
	void MouseButtonLeftReleased();


public:
	FPlayerClickDelegate PlayerClicked;
	FPlayerClickDelegate PlayerClickReleased;
	
};
