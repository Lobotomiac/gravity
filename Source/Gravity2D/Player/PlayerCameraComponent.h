// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "TimerManager.h"

#include "PlayerCameraComponent.generated.h"


/**
 * 
 */
UCLASS()
class GRAVITY2D_API UPlayerCameraComponent : public UCameraComponent
{
	GENERATED_BODY()
	
	UPlayerCameraComponent();
	
	bool bForwardInterpolation;
	//TODO add a bool (placeholder bHasButtonBeenReleased) to check for in forward RotationLerp 
	
	FRotator OriginalRotation, SpawnRotation;
	float LerpTimeElapsed;
	float LerpTimeTotal;
	float LerpTimeInterupted;

	FTimerDelegate TimerDelegate;
	FTimerHandle TimerHandle;

	bool bLerpDebugBool = false;

public:
	UFUNCTION()
	void RotationLerp(const FRotator &_startRotation, const FRotator &_endRotation);

	void SpawnCameraRotation(const float &_lerpDuration, const bool &_forwards);

};
