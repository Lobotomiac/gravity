// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerSpectatorPawn.h"
#include "Components/InputComponent.h"
#include "Player/GravityPlayerController.h"
#include "Player/PlayerCameraComponent.h"
#include "Game/GravityGameMode.h"
#include "Bodies/Sun.h"
#include "Bodies/Planetoid.h"
#include "DrawDebugHelpers.h"
#include "Math/Vector.h"


//TODO add a timed restriction on how quickly can we try to spawn a planetoid (not until the pawn comes back to original position probably)

APlayerSpectatorPawn::APlayerSpectatorPawn()
{
	SpawnLaunchForce = 5.0f;
	CameraComp = CreateDefaultSubobject<UPlayerCameraComponent>(TEXT("CameraComponent"));
	CameraComp->SetupAttachment(RootComponent);
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	//TODO Add a transitionTime Original so we can change the transition duration based on interruptions etc (TransitionTimeElapsed usable?)
	FullTransitionduration = 1.5f;
	TransitionDuration = FullTransitionduration;
	TransitionTimeElapsed = 0.0f;
	bCanStartSpawningProcedure = true;
}

void APlayerSpectatorPawn::BeginPlay()
{
	Super::BeginPlay();

	GravityPC = Cast<AGravityPlayerController>(GetWorld()->GetFirstPlayerController());
	GravityPC->PlayerClicked.BindUObject(this, &APlayerSpectatorPawn::StartSpawnProcedure);
	GravityPC->PlayerClickReleased.BindUObject(this, &APlayerSpectatorPawn::EndSpawnProcedure);
}


void APlayerSpectatorPawn::StartSpawnProcedure()
{
	if (bCanStartSpawningProcedure)
	{
		ResetLerpTimer(); // TODO call it by LerpComponent
		//TODO remove bCanStartSpawningProcedure and replace it with a timer function?
		bCanStartSpawningProcedure = false;
		bSpawning = true;
		FVector SunLocation;
		AGravityGameMode* GameMode = Cast<AGravityGameMode>(GetWorld()->GetAuthGameMode());
		ASun* Sun= GameMode->GetSun();
		// Remembering our position so we can lerp back to it after the spawning
		PreSpawnPawnLocation = GetActorLocation();
		// TODO check this out what happens with the planetoid location etc
		AlignMouseClickWithActor(Sun, SunLocation, PlanetoidSpawnLocation);
		PawnSpawnLocation = GetPawnSpawnPosition(SunLocation, PlanetoidSpawnLocation);

		CameraComp->SpawnCameraRotation(TransitionDuration, bSpawning);

		// Creating a delegate and calling the timer for lerping the pawn to the spawning location
		TransitionDelegate.BindUFunction(this, FName(TEXT("PawnSpawnFVectorLerp")), PreSpawnPawnLocation, PawnSpawnLocation);
		GetWorldTimerManager().SetTimerForNextTick(TransitionDelegate);
	}
}

void APlayerSpectatorPawn::SpawnPlanetoid()
{
	SpawnedPlanetoid = GetWorld()->SpawnActor<APlanetoid>(Planetoid, PlanetoidSpawnLocation, GetActorRotation());

	//TODO check references for SpawnPlanetoid || call it directly after lerp finishes? not to rely on the timer itself but calling it right on the spot if conditions are met???
	// Here we'll be calling the spawning procedure after TransitionDuration to allow our pawn and camera to transition into the spawning position for better experience
	GetWorldTimerManager().SetTimer(SpawnTimerHandle, this, &APlayerSpectatorPawn::UpdateSpawnProcedure, GetWorld()->GetDeltaSeconds(), true, FullTransitionduration); 
}


//TODO figure out why PawnSpawnPositionLerp works for longer than it needs to 
void APlayerSpectatorPawn::PawnSpawnFVectorLerp(const FVector &_locationToTransitionFrom, const FVector &_locationToTransitionTo)
{
	if (TransitionTimeElapsed < TransitionDuration)
	{
		FVector NewLocation = FMath::Lerp(_locationToTransitionFrom, _locationToTransitionTo, TransitionTimeElapsed / TransitionDuration);
		
		SetActorLocation(NewLocation);
		TransitionTimeElapsed += GetWorld()->GetDeltaSeconds();

		GetWorldTimerManager().SetTimerForNextTick(TransitionDelegate);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("PlayerSpectatorPawn lerp finished: %f"), GetWorld()->GetTimeSeconds())

		// TODO set spawn mechanic here (spawnplanetoid())
		// figure how to, if canceled, transfer the transition time to the reverse transition (same duration backwards)
		ResetLerpTimer(); // TODO call it by LerpComponent
		if (!bCanStartSpawningProcedure && !bSpawning)
		{
			bCanStartSpawningProcedure = true;
		}
	}
}


void APlayerSpectatorPawn::UpdateSpawnProcedure()
{
	if (bSpawning)
	{
		IncreasePlanetoidSize();
		TrackSpawnVelocity();
	}
	else
	{
		GetWorldTimerManager().ClearTimer(SpawnTimerHandle);
	}	
}


void APlayerSpectatorPawn::IncreasePlanetoidSize()
{
	float DeltaSeconds = GetWorld()->GetDeltaSeconds();
	FVector PlanetoidScale = SpawnedPlanetoid->GetActorScale();
	PlanetoidScale += FVector(DeltaSeconds, DeltaSeconds, DeltaSeconds);
	SpawnedPlanetoid->SetActorScale3D(PlanetoidScale);
}


void APlayerSpectatorPawn::TrackSpawnVelocity()
{
	FVector PlanetoidLocation, MouseLocation;
	AlignMouseClickWithActor(SpawnedPlanetoid, PlanetoidLocation, MouseLocation);
	DrawDebugLine(GetWorld(), PlanetoidLocation, MouseLocation, FColor::Red, false, 1.0f);
	FVector ForwardVector = PlanetoidLocation - MouseLocation;
	DrawDebugLine(GetWorld(), PlanetoidLocation, PlanetoidLocation + ForwardVector, FColor::Blue, false, 1.0f);
}


void APlayerSpectatorPawn::EndSpawnProcedure()
{	
	// reset the elapsed time as well, it resets the spawning mechanism since it doesn't reset
	ResetLerpTimer(); // TODO call it by LerpComponent

	bSpawning = false;
	
	FVector CurrentPawnLocation = GetActorLocation();
	TransitionDelegate.BindUFunction(this, FName(TEXT("PawnSpawnFVectorLerp")), CurrentPawnLocation, PreSpawnPawnLocation);
	GetWorldTimerManager().SetTimerForNextTick(TransitionDelegate);
	CameraComp->SpawnCameraRotation(TransitionDuration, bSpawning); //TODO remove transition duration

	/* The following section is basically the Spawning force and direction for the spawned planetoid
	 * We set the force, direction and the planetoid mass so it behaves realistic enough regarding real world physics */

	if (SpawnedPlanetoid)
	{
		UStaticMeshComponent* PlanetoidMesh = Cast<UStaticMeshComponent>(SpawnedPlanetoid->GetRootComponent());
		float PlanetoidScale = PlanetoidMesh->GetComponentScale().X;

		// Multiplying by cube of planetoid density to get a fairly accurate mass 
		float PlanetoidDensity = SpawnedPlanetoid->GetPlanetoidDensity();
		float PlanetoidMass = PlanetoidScale * (PlanetoidDensity * PlanetoidDensity * PlanetoidDensity);
		// set mass override in kg
		PlanetoidMesh->SetMassOverrideInKg(NAME_None, PlanetoidMass, true);
		PlanetoidMesh->SetSimulatePhysics(true);

		// This launches the planetoid at a certain velocity, with its strength dictated by SpawnLaunchForce
		FVector PlanetoidLocation, MouseLocation;
		AlignMouseClickWithActor(SpawnedPlanetoid, PlanetoidLocation, MouseLocation);
		FVector ForwardVector = PlanetoidLocation - MouseLocation;
		ForwardVector *= SpawnLaunchForce;
		PlanetoidMesh->AddImpulse(ForwardVector, NAME_None, true);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No Planetoid to spawn. Planetoid = nullptr"))
	}	
}


// This function gets us the mouse click intersection with a plane originating from the Sun actor
void APlayerSpectatorPawn::AlignMouseClickWithActor(const AActor *_actor, FVector &_actorLocation, FVector &_clickLocation)
{
	FVector WorldPosition, WorldDirection, ActorExtent;
	_actor->GetActorBounds(false, _actorLocation, ActorExtent);
	
	// To know where our mouse pointer points to in the world
	GravityPC->DeprojectMousePositionToWorld(WorldPosition, WorldDirection);
	WorldDirection *= 100000.0f;
	// An imaginary plan with the center at the Players location TODO add why
	FPlane XYPlane = FPlane(_actorLocation, FVector(0.0f, 0.0f, 1.0f)); // Set SunLocation instead of PawnLocation
	// Finding the exact point of the mouse click on the imaginary plane
	_clickLocation = FMath::LinePlaneIntersection(WorldPosition, WorldPosition + WorldDirection, XYPlane);	
}


FVector APlayerSpectatorPawn::GetPawnSpawnPosition(const FVector &_sunLocation, const FVector &_clickLocation)
{
	float ClickToSunDistance = FVector::Dist(_sunLocation, _clickLocation);
	FVector ClickToSunDirection = _clickLocation - _sunLocation;
	FVector MidPoint = _clickLocation - (ClickToSunDirection / 2);

	DrawDebugBox(GetWorld(), MidPoint, FVector(5.0f, 5.0f, 5.0f), FColor::Orange, false, 5.0f, 0, 5.0f);	

	FVector SpawnProcedurePawnLocation = MidPoint;
	SpawnProcedurePawnLocation.Z += ClickToSunDistance * 2;
	
	DrawDebugBox(GetWorld(), SpawnProcedurePawnLocation, FVector(5.0f, 5.0f, 5.0f), FColor::Orange, false, 5.0f, 0, 5.0f);
	return SpawnProcedurePawnLocation;
}


void APlayerSpectatorPawn::ResetLerpTimer()
{
	// for setting the transition duration to default time if spawning has ended (or to set it to the time it got interrupted for backwards lerping)
	TransitionTimeElapsed > 0.0f ? TransitionDuration = TransitionTimeElapsed : TransitionDuration = FullTransitionduration; 
	TransitionTimeElapsed = 0.0f;

	if (!bSpawning)
	{
		TransitionDuration = FullTransitionduration;
	}
	GetWorldTimerManager().ClearTimer(TransitionTimerHandle);

	TransitionDelegate.Unbind();
}

void APlayerSpectatorPawn::Tick(float DeltaSeconds)
{
	//UE_LOG(LogTemp, Warning, TEXT("PawnRotation:		%s"), *GetActorRotation().ToString())

	//UE_LOG(LogTemp, Warning, TEXT("GetViewRotation:	%s"), *GetViewRotation().ToString())
	
	//UE_LOG(LogTemp, Warning, TEXT("GetControlRotation:	%s"), *GetControlRotation().ToString())

	//UE_LOG(LogTemp, Warning, TEXT("bCanStartSpawningProcedure:	%s"), bCanStartSpawningProcedure ? TEXT("true") : TEXT("false"))

	//UE_LOG(LogTemp, Warning, TEXT("CameraComp Rotation:%s"), *CameraComp->GetComponentRotation().ToString())

	//UE_LOG(LogTemp, Warning, TEXT("pawnLocation:		%s"), *GetActorLocation().ToString())

}