// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpectatorPawn.h"
#include "PlayerSpectatorPawn.generated.h"

/**
 * 
 */


 // Forward declaration so we don't need to include it in
class AGravityPlayerController;
class UPlayerCameraComponent;
class APlanetoid;


UCLASS()
class GRAVITY2D_API APlayerSpectatorPawn : public ASpectatorPawn
{
	GENERATED_BODY()

	APlayerSpectatorPawn();

	virtual void BeginPlay() override;
	
	
private:
	AGravityPlayerController* GravityPC = nullptr;
	UPROPERTY(VisibleAnywhere)
	UPlayerCameraComponent* CameraComp = nullptr;

	UFUNCTION()
	void Tick(float DeltaSeconds);

	UFUNCTION()
	void StartSpawnProcedure();

	void SpawnPlanetoid();

	void UpdateSpawnProcedure();
	void IncreasePlanetoidSize();
	void TrackSpawnVelocity();

	UFUNCTION()
	void EndSpawnProcedure();

	void AlignMouseClickWithActor(const AActor *_actor, FVector &_actorLocation, FVector &_clickLocation);
	FVector GetPawnSpawnPosition(const FVector &_sunLocation, const FVector &_clickLocation);

	void ResetLerpTimer();
	
	UPROPERTY(EditDefaultsOnly, Category = "Spawning", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<APlanetoid> Planetoid;
	
	APlanetoid* SpawnedPlanetoid = nullptr;
	FTimerHandle SpawnTimerHandle;
	FTimerHandle TransitionTimerHandle;
	
	// It seems that 0.5 is a good force modifier for the launching vector
	UPROPERTY(EditAnywhere, Category = "Spawning", meta	= (AllowPrivateAccess = "true"))
	float SpawnLaunchForce;
	
	UPROPERTY(EditAnywhere, Category = "Spawning")
	float FullTransitionduration;
	float TransitionDuration;
	float TransitionTimeElapsed;
	FVector PreSpawnPawnLocation;
	FVector PawnSpawnLocation;
	FVector PlanetoidSpawnLocation;

	void ResetTransitionTimeElapsed() { TransitionTimeElapsed = 0.0f; }

	// We'll be using this to mark start and end of spawning procedure
	bool bSpawning;
	bool bCanStartSpawningProcedure;
	

public:
	UFUNCTION()
	void PawnSpawnFVectorLerp(const FVector &_locationToTransitionFrom, const FVector &_locationToTransitionTo);
	FTimerDelegate TransitionDelegate;


};
