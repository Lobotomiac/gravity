// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCameraComponent.h"
#include "TimerManager.h"
#include "Math/UnrealMathUtility.h"



UPlayerCameraComponent::UPlayerCameraComponent()
{
	bUsePawnControlRotation = true;
	bForwardInterpolation = true;
	LerpTimeElapsed = 0.0f;
}


void UPlayerCameraComponent::SpawnCameraRotation(const float &_lerpDuration, const bool &_forwards)
{
	LerpTimeTotal = _lerpDuration;
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	bForwardInterpolation = _forwards;
	bUsePawnControlRotation = false;

	LerpTimeElapsed = 0.0f;

	if (_forwards)
	{
		OriginalRotation = GetComponentRotation();
		SpawnRotation = (FRotator(-90.0f, OriginalRotation.Yaw, OriginalRotation.Roll));
		TimerDelegate.BindUFunction(this, FName(TEXT("RotationLerp")), OriginalRotation, SpawnRotation);
	}
	else
	{
		SpawnRotation = GetComponentRotation();
		TimerDelegate.BindUFunction(this, FName(TEXT("RotationLerp")), SpawnRotation, OriginalRotation);
	}
	GetWorld()->GetTimerManager().SetTimerForNextTick(TimerDelegate);
}


// TODO create a lerping class with overrides for both FRotator and FVector!!!!!
void UPlayerCameraComponent::RotationLerp(const FRotator &_startRotation, const FRotator &_endRotation)
{
	if (LerpTimeElapsed < LerpTimeTotal)
	{
		FRotator NewRotation = FMath::Lerp(_startRotation, _endRotation, LerpTimeElapsed / LerpTimeTotal);
		SetWorldRotation(NewRotation);
		LerpTimeElapsed += GetWorld()->GetDeltaSeconds();
		
		GetWorld()->GetTimerManager().SetTimerForNextTick(TimerDelegate);
		/*if (bLerpDebugBool)
		{
			UE_LOG(LogTemp, Warning, TEXT("PlayerCameraComponent lerp working: %f"), GetWorld()->GetTimeSeconds())

		}*/
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("PlayerCameraComponent lerp finished: %f"), GetWorld()->GetTimeSeconds())
		LerpTimeElapsed = 0.0f;
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
		if (!bForwardInterpolation)
		{	
			bUsePawnControlRotation = true;
		}
		bLerpDebugBool = !bLerpDebugBool;
	}
}


// ViewRotation is same as camera rotation just calculated differently CHECK IT OUT

// TODO figure out how to synchronize the camera rotation with bPawnControllRotation