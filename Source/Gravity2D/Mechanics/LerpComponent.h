// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "LerpComponent.generated.h"

//TODO create a delegate for triggering planetoid spawning on PlayerSpectatorPawn!!!!

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GRAVITY2D_API ULerpComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ULerpComponent();

	//TODO create an InitiateLerp function to start the whole sequence
	void InitiateLerp(FVector _ActorLocationEnd, FRotator _CameraRotation);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void LerpOwnerLocation();
	void LerpCameraRotation();

	FTimerDelegate LerpTransitionDelegate; //TODO perhaps another delegate for separate functions? not sure why but we'll get to that later

	float MaxTransitionDuration;
	float TransitionDuration;
	float TransitionTimeElapsed;

	void IncreaseTransitionTimeElapsed() { TransitionTimeElapsed += GetWorld()->GetDeltaSeconds(); }
	void ResetTransitionTimeElapsed() { TransitionTimeElapsed = 0.0f; }
	void ResetLerpTimer();
	void SetTransitionDuration() { TransitionTimeElapsed > 0.0f ? TransitionDuration = TransitionTimeElapsed : TransitionDuration = MaxTransitionDuration; }

	void UpdateTimeAndLocation();

	FVector OwnerLocationStart;
	FVector OwnerLocationEnd;
	FRotator CameraRotationStart;
	FRotator CameraRotationEnd;
};
