// Fill out your copyright notice in the Description page of Project Settings.


#include "Mechanics/LerpComponent.h"

// Sets default values for this component's properties
ULerpComponent::ULerpComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	float MaxTransitionDuration = 1.5;
	float TransitionDuration = MaxTransitionDuration;
	float TransitionTimeElapsed = 0.0f;
}

// Called when the game starts
void ULerpComponent::BeginPlay()
{
	Super::BeginPlay();
}

void ULerpComponent::InitiateLerp(FVector _ActorLocationEnd, FRotator _CameraRotation)
{
	OwnerLocationStart = GetOwner()->GetActorLocation();
	OwnerLocationEnd = _ActorLocationEnd;

	CameraRotationStart // Make an Getter or something for the camera component
}

void ULerpComponent::LerpOwnerLocation(float _LerpAlpha)
{
		FVector NewLocation = FMath::Lerp(OwnerLocationStart, OwnerLocationEnd, _LerpAlpha);
		GetOwner()->SetActorLocation(NewLocation)
}

void ULerpComponent::LerpCameraRotation (float _LerpAlpha)
{
	FRotator NewRotation = FMath:Lerp(CameraRotationStart, CameraRotationEnd, _LerpAlpha);
	
}

void ULerpComponent::UpdateTimeAndLocation() 
{
	float LerpAlpha = TransitionTimeElapsed / TransitionDuration;
	if (LerpAlpha < 1) {
		LerpOwnerLocation
		IncreaseTransitionTimeElapsed();
		GetWorldTimerManager().SetTimerForNextTick(TransitionDelegate);
	} else {
		ResetLerpTimer ();
		UE_LOG(LogTemp, Warning, TEXT("ULerpComponent::LerpOwnerLocation finished at: %f"), GetWorld()->GetTimeSeconds());
	}
}

void ULerpComponent::ResetLerpTimer()
{
	SetTransitionDuration();
	ResetTransitionTimeElapsed();
	// FTimerManager::ClearAllTimersForObject perhaps?

	if (!bSpawning)
	{
		TransitionDuration = FullTransitionduration;
	}
	//GetWorldTimerManager().ClearTimer(TransitionTimerHandle);

	//TransitionDelegate.Unbind();
}

