// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Bodies/Planetoid.h"
#include "GravityGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class GRAVITY2D_API UGravityGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public :

	TArray<const APlanetoid*> Planetoids;

	void AddPlanetoid(const APlanetoid* _planetoid);
	void RemovePlanetoid(const APlanetoid* _planetoid);
};
