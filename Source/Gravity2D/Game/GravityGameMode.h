// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "GravityGameMode.generated.h"

/**
 * 
 */

 class ASun;

UCLASS()
class GRAVITY2D_API AGravityGameMode : public AGameMode
{
	GENERATED_BODY()

	AGravityGameMode();
	
	ASun* Sun = nullptr;

public:
	
	void SetSun(ASun* SunPtr) { Sun = SunPtr; };
	ASun* GetSun() const { if (Sun) return Sun; return nullptr; };
	
};
